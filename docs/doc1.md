---
id: reactivex
title: Reactive X
sidebar_label: Conceitos
---

[*Reactive Extensions (ReactiveX ou Rx)*](http://reactivex.io/) é um conjunto de bibliotecas que segue os princípios da programação reativa. Essas bibliotecas fornecem um conjunto de interfaces e métodos que ajudam os desenvolvedores a escrever códigos limpos e simples.

## Programação reativa

Programação reativa é programar com fluxos(streams) de dados assíncronos. Assíncrono significa que não se realiza ao mesmo tempo ou no mesmo ritmo de desenvolvimento (em relação a outra coisa).

Isso é importante devido o crescimento da Internet e a demanda por tempo real, a programação precisa ser dinâmica, ou seja; diferente da forma tradicional de desenvolvimento.

A forma tradicional de programar/desenvolver, por exemplo, cria várias tarefas e elas se comunicam em tempos determinados, com respostas determinadas sem escalabilidade caso ocorra alguma falha, alguma demanda maior, são "rígidas", seguem regras diretas. Isto funcionava e continua sendo utilizada até hoje, entretanto esta "lógica" não é mais compatível com a necessidade atual.

No método tradicional é como preparar a mistura do cimento, depois que este enviou os dados dizendo que está preparado, é chamada a função de construir a parede, depois o reboco, esperar um tempo determinado, passado o período é chamada a função de pintar e se faltar o pintor? Ferrou tudo, deu erro no sistema! Ou se a comunicação falhar? Erro também, sistema trava, geralmente é preciso refazer tudo!

Na programação reativa isso tudo ocorre, mas de forma inteligente, interligada em paralelo, sem seguir aquela ordem cronológica e linear, como por exemplo: a parede esta construída, falta o reboco, tempo pra secar, depois pintar. Não tem pintor, falha nesta tarefa, o sistema não irá travar, reconhece que é possível continuar outras partes da construção em paralelo como o chão, botar o piso, enquanto fica em espera a tarefa de pintar, sem ter que recomeçar tudo outra vez, concluir depois a pintura, sem ter sido interrompido o sistema pelo "erro".

É por aí a lógica, parece meio óbvio se for pensar, mas os sistemas ainda são pouco inteligentes, precisam ser devidamente programados para estarem aptos a gerenciar falhas, rotinas diferentes, seguir caminhos alternativos, muitos acessos, funcionar em tempo real e sempre estar online/ativo!

### Os 4 pilares

![4 pilares](assets/4-pilares-reactive.png)

- **Elástico**: Reage à demanda/carga: aplicações podem fazer uso de múltiplos núcleos e múltiplos servidores;
- **Resiliente**: Reage às falhas; aplicações reagem e se recuperam de
falhas de software, hardware e de conectividade;
- **Message Driven**: Reage aos eventos (event driven): em vez de compor
aplicações por múltiplas threads síncronas, sistemas são compostos de gerenciadores de eventos assíncronos e não bloqueantes;
- **Responsivo**: Reage aos usuários: aplicações que oferecem interações
ricas e “tempo real” com usuários.

## Pra que

Alguns casos de uso onde a programação reativa pode fazer sentido são:

- Eventos de UI (movimento do mouse, clicks de botão, etc), programadores estão acostumados a lidar com eventos de componentes, então não há nada fundamentalmente diferente. As vantagens do paradigma reativo surgem com o conjunto de operadores disponíveis para manipulação dos eventos, tornando simples tarefas como, por exemplo, throttle de múltiplos clicks ou propagação de eventos para interfaces hiper-interativas;

- Chamadas para serviços externos (REST, RPC, etc): operações realizadas sobre HTTP são bloqueantes por natureza; ao fornecer uma simplificação para o código assíncrono, a programação reativa pode ajudá-lo a desbloquear o código de cliente HTTP, mas essa é a parte mais simples. Em arquiteturas de serviços distribuídos (como microserviços), é comum um código de back-end construir uma composição entre várias chamadas dependentes (o serviço “a” é invocado, e a resposta é utilizada como parâmetro para invocar o serviço “b”, e sucessivamente). Frameworks reativos podem ajudar a orquestrar chamadas dependentes de maneira natural, com a vantagem de **NÃO** bloquear o código no cliente.

- Consumo de mensagens: processamento de mensagens com alta concorrência é um caso de uso comum. Declaradamente, frameworks como o próprio RxJava, Reactor ou o Akka alegam serem capazes de processar milhões de mensagens por segundo na JVM sem esforço. Sendo isso verdade ou não, em maior ou menor grau, novamente sua implementação pode se beneficiar das ferramentas disponíveis nesses frameworks para construir um pipeline de consumo de eventos/mensagens de maneira muito simples.

- Abstração sobre processamento assíncrono: esse detalhe vai depender da ferramenta e da linguagem que estiver utilizando , mas um dos pontos-chave dos frameworks reativos é fornecer uma fundação simples para processamento assíncrono, desafogando sua aplicação dos detalhes mais complexos envolvendo multithreading e permitindo que seu código possa se concentrar na lógica de manipulação dos eventos.

## Observer Pattern

O padrão Observer oferece um modelo de assinatura no qual os objetos se inscrevem em um evento e são notificados quando o evento ocorre. Esse padrão é a base da programação orientada a eventos, incluindo o JavaScript. O padrão Observer facilita o bom design orientado a objetos e promove um acoplamento flexível.

Quando trabalhamos com JavaScript, acabamos escrevendo muitos manipuladores de eventos, que são funções que serão notificadas quando um determinado evento for disparado. Essas notificações, opcionalmente, recebem um argumento de evento com detalhes sobre o evento.

O paradigma de evento e manipulador de eventos em JavaScript é a manifestação do padrão de design do Observer. Outro nome para o padrão Observer é Pub/Sub, abreviação de Publicação/Assinatura.

## Iterator Pattern

O padrão Iterator permite que os clientes realizem loop de forma efetiva em uma coleção de objetos.

Uma tarefa de programação comum é percorrer e manipular uma coleção de objetos. Essas coleções podem ser armazenadas como uma matriz ou talvez algo mais complexo, como uma estrutura de árvore ou gráfico. Além disso, podemos precisar acessar os itens na coleção em uma determinada ordem.

O padrão Iterator resolve esse problema separando a coleção de objetos da passagem desses objetos implementando um iterador especializado.

Muitas linguagens possuem Iterators embutidos suportando construções do tipo ‘for-each’ e interfaces IEnumerable e IEnumerator. No entanto, o JavaScript suporta apenas loop básico na forma de instruções for, for-in, while e do while.

O padrão Iterator permite que os desenvolvedores de JavaScript projetem construções de looping que sejam muito mais flexíveis e sofisticadas.

## RxJS

- Uma biblioteca javascript que traz o conceito de "programação reativa" para a web, para transformar, compor e consultar fluxos de dados. Desde simples matrizes de valores, a séries de eventos e fluxos complexos de dados.
- É uma biblioteca para composição de programas assíncronos e baseados em eventos usando sequências observáveis
- Fornece um tipo de núcleo, os tipos de satélites do Observer Pattern e operadores inspirados em Array tais como, map, filter, reduce, every, etc... Para permitir manipular eventos assíncronos em coleções de dados.

O RxJS combina o padrão Observer com o padrão Iterator e a programação funcional para preencher a necessidade de uma maneira ideal de gerenciar seqüências de eventos.

Existem conceitos essenciais que resolvem o gerenciamento de eventos assíncronos e são:

- **Observable**: Representa a ideia de uma coleção invocável de valores ou eventos futuros (Semelhante a uma Promise).
- **Observer**: É uma coleção de retornos de chamada que sabe como escutar os valores entregue pelo Observable.
- **Subscription**: Representa a execução de um Observable, é principalmente útil para cancelar a execução.
- **Operators**: São funções puras que permitem um estilo de programação funcional de lidar com coleções com operações como map, filter, concat, reducet, etc.
- **Schedulers**: São despachantes centralizados para controlar a simultaneidade, permitiindo nos coordenar quando o cálculo ocorre em, e. setTimeout ou requestAnimationFrame ou outros.



## Refêrencias

- [Programação reativa](https://pt.stackoverflow.com/questions/55332/o-que-%C3%A9-reactive-programming-programa%C3%A7%C3%A3o-reativa)
- [Pra que?](https://elo7.dev/programacao-reativa/)
- [Play list](https://www.youtube.com/playlist?list=PLJYZJYNervmPUrNb-mHh7ct8c21R8nWqn)
- [RxJS](https://bognarjunior.wordpress.com/2018/09/30/rxjs-programacao-reativa-em-javascript/)
