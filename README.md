# Gitlab CI

## Script

É a única palavra chave requirida em um job.

## Job

Um job contente um única clausa que se faz necessária que é a script, quem executa os comandos

- Precisa ter um nome único
- É pelo por um runner
- Não pode ter palavras reservadas (image, services, stages, types, before_script, after_script, variables, cache)

## image

Podemos definir simplementes a imagem que queremos utilizar nos nossos jobs

## Stages

Quando não é colocado nada, o gitlab entende que temos os stages default, build test e deploy.

## Artifcts

Para expor os diretórios, sendo possível fazer download do conteúdo que o gitlab compilou. Pode ser passado artifcts para outros jobs (ou utilizar o cache). Conseguimos definir nomes para os Artificts existem váriaveis do gitlab que pode auxiliar.
Os artificts tem parâmetros para ser passados, untracked (podemos exporta valores ignorados semelhantes ao node_modules), when (pode ser quando falha, sucesso ou ambos), expire_in (pode se definido quanto tempo o artifcts terá de tempo de vida)

## Cache

Ele é parecido com o artifcts, é utilizado quando é algo "bom baixo dos panos", definimos um diretório para ficar em cache e todos os jobs conseguem fazer acessar ele.
Pode cachear mais de um diretório e podemos dar um alias para o mesmo, se não é colocado a key os cache são sobrescrito. Policy, existe três opções, pull (), push()

## Before script e After script

São executado antes ou depois, podem ser definido no contexto do job ou global, se ficar no contexto global será executado em todos os jobs.
Todo Job que tem um script local irá ignorar o global

## Only e Except

Define o cenário que será executado o pipeline

## Variables

Local ou global

## Git Strategy

Se é clone, fetch ou nada. O Git fetch é mais rápido pois ele utiliza o workspace, o clone é mais demorado pois baixa tudo do repositório.


## Ferramentas interessantes

- [Broken link Checker](https://www.npmjs.com/package/broken-link-checker)
- [Wait on](https://www.npmjs.com/package/wait-on)
- [Snyk](https://snyk.io/)
